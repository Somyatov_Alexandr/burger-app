import React, {Component} from 'react';
import axios from '../../axios-orders';
import OrderItem from "./OrderItem/OrderItem";
import Spinner from "../../components/UI/Spinner/Spinner";

class Orders extends Component {
  state = {
    orders: [],
    loading: true
  };

  componentDidMount(){
    axios.get('/orders.json').then(response => {
      const orders = [];

      for (let key in response.data) {
        orders.push({...response.data[key], id: key});
      }

      this.setState({orders, loading: false});
    }).catch(() => {
      this.setState({loading: false});
    })
  }

  render() {
    let orders = this.state.orders.map(order => (
        <OrderItem key={order.id}
                   ingredients={order.ingredients}
                   price={order.price}
        />
    ));

    if (this.state.loading) {
      orders = <Spinner />;
    }

    return orders;

  }
}

export default Orders;
