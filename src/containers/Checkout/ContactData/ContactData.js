import React, {Component} from 'react';
import Button from "../../../components/UI/Button/Button";
import axios from '../../../axios-orders';

import './ContactData.css';
import Spinner from "../../../components/UI/Spinner/Spinner";


class ContactData extends Component{
  state = {
    name: '',
    email: '',
    street: '',
    postal: '',
    loading: false
  };

  valueChanged = event => {
    const name = event.target.name;
    this.setState({
      [name]: event.target.value
    })
  };

  orderHandler = event => {
    event.preventDefault();

    this.setState({loading: true});

    const order = {
      ingredients: this.props.ingredients,
      price: this.props.price,
      customer: {
        name: this.state.name,
        email: this.state.email,
        street: this.state.street,
        postal: this.state.postal
      }
    };

    axios.post('/orders.json', order).finally(() => {
      this.setState({loading: false});
      this.props.history.push('/');
    });
  };

  render() {

    let form = (
        <form onSubmit={this.orderHandler}>
          <input value={this.state.name} onChange={this.valueChanged} type="text" name="name" className="Input" placeholder="Введите Имя"/>
          <input value={this.state.email} onChange={this.valueChanged} type="text" name="email" className="Input" placeholder="Введите Email"/>
          <input value={this.state.street} onChange={this.valueChanged} type="text" name="street" className="Input" placeholder="Введите Улицу"/>
          <input value={this.state.postal} onChange={this.valueChanged} type="text" name="postal" className="Input" placeholder="Введите почтовый индекс"/>
          <Button btnType="Success">ЗАКАЗАТЬ</Button>
        </form>
    );
    if (this.state.loading) {
      form = <Spinner/>
    }

    return (
        <div className="ContactData">
          <h4>Введите свои контактные данные</h4>
          {form}
        </div>
    );
  }
}

export default ContactData;