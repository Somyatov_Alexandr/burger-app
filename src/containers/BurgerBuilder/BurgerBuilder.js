import React, {Component, Fragment} from 'react';
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import {connect} from 'react-redux'
import {addIngerdient, removeIngerdient} from "../../store/actions/burgerAction";

const INGREDIENTS_PRICE = {
  salad: 5,
  meat: 50,
  cheese: 20,
  bacon: 30
};

class BurgerBuilder extends Component {
  state = {
    totalPrice: 20,
    purchasable: false,
    purchasing: false
  };


  updatePurchaseState = ingredients => {
    // [1, 0, 1, 1]
    const sum = Object.keys(ingredients).map(igKey => {
      return ingredients[igKey];
    }).reduce( (acc, item) => {
      return acc += item;
    }, 0);

    this.setState({
      purchasable: sum > 0
    })

  };

  purchaseHandler = () => {
    this.setState({purchasing: true});
  };
  purchaseCancelHandler = () => {
    this.setState({purchasing: false});
  };

  purchaseContinueHandler = () => {
    const queryParams = [];

    for (let key in this.state.ingredients) {
      // [ bacon=1, meat=2, cheese=1, salad=0]
      queryParams.push(encodeURIComponent(key) + '='
          + encodeURIComponent(this.state.ingredients[key]))
    }

    queryParams.push('price=' + this.state.totalPrice);

    //?bacon=1&meat=2&cheese=1&salad=0&price=100
    const queryString = queryParams.join('&');

    this.props.history.push({
      pathname: '/checkout',
      search: '?' + queryString
    });
  };




  render() {

    const disabledInfo = {...this.props.ingredients};

    // bacon: true, meat: true, cheese: false, salad: false

    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0;
    }

    return (
      <Fragment>
        <Modal show={this.state.purchasing} closed={this.purchaseCancelHandler}>
          <OrderSummary
              ingredients={this.props.ingredients}
              price={this.state.totalPrice}
              purchaseCancelled={this.purchaseCancelHandler}
              purchaseContinued={this.purchaseContinueHandler}
          />

        </Modal>
        <Burger ingredients={this.props.ingredients}/>
        <div>
        </div>
        <BuildControls
            price={this.state.totalPrice}
            ingredients={this.props.ingredients}
            ingredientAdded={this.props.onIngredientAdded}
            ingredientRemoved={this.props.onIngredientRemoved}
            disabled={disabledInfo}
            purchasable={this.state.purchasable}
            ordered={this.purchaseHandler}

        />
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    ingredients: state.ingredients
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onIngredientAdded: (igName) => dispatch(addIngerdient(igName)),
    onIngredientRemoved: (igName) => dispatch(removeIngerdient(igName))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(BurgerBuilder);
