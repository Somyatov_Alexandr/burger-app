import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://red0919-89360.firebaseio.com'
});

export default instance;