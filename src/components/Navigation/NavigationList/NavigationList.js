import React from 'react';
import {NavLink} from "react-router-dom";

import './NavigationList.css';

const NavigationList = () => {
  return (
      <ul className="NavigationList">
        <li className="NavigationItem">
          <NavLink to="/" exact>Burger Builder</NavLink>
        </li>
        <li className="NavigationItem">
          <NavLink to="/orders" exact>Orders</NavLink>
        </li>
      </ul>
  );
};

export default NavigationList;
