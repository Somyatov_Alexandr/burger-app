import React from 'react';
import burgerLogo from './burger_logo.png';
import './Logo.css';

const Logo = () => {
  return <div className="Logo"><img src={burgerLogo} alt="Burger logo"/></div>
};

export default Logo;
