import React from 'react';
import Burger from "../../Burger/Burger";
import Button from "../../UI/Button/Button";

import './CheckoutSummary.css';


const CheckoutSummary = props => {
  return (
      <div className="CheckoutSummary">
        <h1>Вы довольны своим заказом?</h1>
        <div style={{width: '100%', margin: 'auto'}}>
          <Burger ingredients={props.ingredients}/>
        </div>
        <Button btnType="Danger" clicked={props.checkoutCancelled}>ОТМЕНА</Button>
        <Button btnType="Success" clicked={props.checkoutContinued}>ПРОДОЛЖИТЬ</Button>
      </div>
  );
};

export default CheckoutSummary;

