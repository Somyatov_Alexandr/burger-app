import React from 'react';
import './BuildControls.css';
import BuildControl from "./BuildControl/BuildControl";

const BuildControls = props => {
  return (
      <div className="build-controls">
        <h3>Total price: {props.price}</h3>
        {Object.keys(props.ingredients).map(igKey => {
          return <BuildControl
              key={igKey}
              type={igKey}
              added={() => props.ingredientAdded(igKey)}
              removed={() => props.ingredientRemoved(igKey)}
              disabled={props.disabled[igKey]}
          />;
        })}
        <button
            className="OrderButton"
            disabled={!props.purchasable}
            onClick={props.ordered}
        >ORDER NOW</button>
      </div>
  );
};

export default BuildControls;