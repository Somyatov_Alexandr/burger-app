import React from 'react';
import './Ingredient.css';

const Ingredient = props => {
  switch (props.type) {
    case 'bread-top':
      return (
          <div className="bread-top">
            <div className="seeds1"/>
            <div className="seeds2"/>
          </div>
      );
    case 'bread-bottom':
      return <div className="bread-bottom"/>;
    case 'meat':
      return <div className="meat"/>;
    case 'bacon':
      return <div className="bacon"/>;
    case 'cheese':
      return <div className="cheese"/>;
    case 'salad':
      return <div className="salad"/>;


    default:
      return null;
  }
};

export default Ingredient;