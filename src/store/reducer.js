import * as actionTypes from './actions/actionTypes';

const initialState = {
  ingredients: {
    bacon: 0,
    cheese: 0,
    meat: 0,
    salad: 0
  },
  totalPrice: 20,
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.ADD_INGREDIENT:
      return {
          ...state,
        ingredients: {
          ...state.ingredients,
          [action.igName]: state.ingredients[action.igName] + 1
        }
      };

    case actionTypes.REMOVE_INGREDIENT:
      return {
        ...state,
        ingredients: {
          ...state.ingredients,
          [action.igName]: state.ingredients[action.igName] - 1
        }
      };

    default:
      return state;
  }


};

export default reducer;