import * as actionTypes from './actionTypes';


export const addIngerdient = igName => {
  return {type: actionTypes.ADD_INGREDIENT, igName}
};

export const removeIngerdient = igName => {
  return {type: actionTypes.REMOVE_INGREDIENT, igName}
};